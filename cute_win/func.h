#ifndef _FUNC_H_
#define _FUNC_H_

#ifndef WIN32

int  _tolower(int c)
{
    if (c >= 'A' && c <= 'Z')
    {
        return c + 'a' - 'A';
    }
    else
    {
        return c;
    }
}

#endif

int htoi(const char s[],int start,int len)
{
    int i,j;
    int n = 0;
    if (s[0] == '0' && (s[1]=='x' || s[1]=='X')) //判断是否有前导0x或者0X
    {
        i = 2;
    }
    else
    {
        i = 0;
    }
    i+=start;
    j=0;
    for (; (s[i] >= '0' && s[i] <= '9')
            || (s[i] >= 'a' && s[i] <= 'f') || (s[i] >='A' && s[i] <= 'F'); ++i)
    {
        if(j>=len)
        {
            break;
        }
        if (_tolower(s[i]) > '9')
        {
            n = 16 * n + (10 + _tolower(s[i]) - 'a');
        }
        else
        {
            n = 16 * n + (_tolower(s[i]) - '0');
        }
        j++;
    }
    return n;
}

typedef struct {
    char **str; // the PChar of string array
    size_t num; // the number of string
} IString;

int Split(char *src, char *delim, IString *istr) // split buf
{
    int i;
    char *str = NULL, *p = NULL;
    
    (*istr).num = 1;
    str = (char *)calloc(strlen(src) + 1, sizeof(char));
    if (str == NULL)
        return 0;
    (*istr).str = (char **)calloc(1, sizeof(char *));
    if ((*istr).str == NULL)
        return 0;
    strcpy(str, src);
    
    p = strtok(str, delim);
    (*istr).str[0] = (char *)calloc(strlen(p) + 1, sizeof(char));
    if ((*istr).str[0] == NULL)
        return 0;
    strcpy((*istr).str[0], p);
    for (i = 1; p = strtok(NULL, delim); i++) {
        (*istr).num++;
        (*istr).str = (char **)realloc((*istr).str, (i + 1) * sizeof(char *));
        if ((*istr).str == NULL)
            return 0;
        (*istr).str[i] = (char *)calloc(strlen(p) + 1, sizeof(char));
        if ((*istr).str[0] == NULL)
            return 0;
        strcpy((*istr).str[i], p);
    }
    free(str);
    str = p = NULL;
    
    return 1;
}

#endif


#define EPR                 fprintf(stderr,
#define ERR(str, chr)       if(opterr){EPR "%s%c\n", str, chr);}
int     opterr = 1;
int     optind = 1;
int    optopt;
char    *optarg;
 
int
getopt (int argc, char *const argv[], const char *opts)
{
    static int sp = 1;
    int c;
    char *cp;
 
    if (sp == 1)
        if (optind >= argc ||
           argv[optind][0] != '-' || argv[optind][1] == '\0')
            return -1;
        else if (strcmp(argv[optind], "--") == 0) {
            optind++;
            return -1;
        }
    optopt = c = argv[optind][sp];
    if (c == ':' || (cp=strchr(opts, c)) == 0) {
        ERR (": illegal option -- ", c);
        if (argv[optind][++sp] == '\0') {
            optind++;
            sp = 1;
        }
        return '?';
    }
    if (*++cp == ':') {
        if (argv[optind][sp+1] != '\0')
            optarg = &argv[optind++][sp+1];
        else if (++optind >= argc) {
            ERR (": option requires an argument -- ", c);
            sp = 1;
            return '?';
        } else
            optarg = argv[optind++];
        sp = 1;
    } else {
        if (argv[optind][++sp] == '\0') {
            sp = 1;
            optind++;
        }
        optarg = 0;
    }
    return c;
}
