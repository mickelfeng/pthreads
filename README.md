# pthreads_study

关于编译时出现 对‘pthread_create’未定义的引用 之类的错误的解决：由于pthread库不是Linux系统默认的库，
连接时需要使用库libpthread.a,所以在使用pthread_create创建线程时，在编译中要加-lpthread参数:

gcc -o pthread -lpthread pthread.c


pthread_join函数：

```
函数pthread_join用来等待一个线程的结束。
函数定义： int pthread_join(pthread_t thread, void **retval);
描述 ：
pthread_join()函数，以阻塞的方式等待thread指定的线程结束。当函数返回时，被等待线程的资源被收回。如果进程已经结束，那么该函数会立即返回。并且thread指定的线程必须是joinable的。
```

### Book 《pthreads programming》

[ 学习pthreads](http://blog.csdn.net/helei001/article/category/2458613)

[[Linux]pthread学习笔记](http://www.cnblogs.com/xfiver/archive/2013/01/23/2873725.html)

[Linux多线程Pthread学习小结](http://blog.csdn.net/ithomer/article/details/6063067)
