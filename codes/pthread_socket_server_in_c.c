#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


#if defined(WIN32) && !defined(__cplusplus)  
#define inline __inline  
#endif  

#ifndef WIN32

#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>

#else
#define FD_SETSIZE 65535
#include<ws2tcpip.h>

#endif

#include <stdarg.h>  
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#pragma comment(lib, "pthreadVC2.lib")  //必须加上这句 

char *http_res_hdr_tmpl = {
		"HTTP/1.1 200 OK\r\nServer: Mickel's Server 0.1\r\nConnection: close\r\nContent-Type: text\/html\r\nContent-Length: 12\r\n\r\nhello,world!"
	};

#define SOCKTESTING
#define BACKLOG 65535


#ifdef _MSC_VER  
#define snprintf c99_snprintf

inline int c99_vsnprintf(char* str, size_t size, const char* format, va_list ap)  
{  
    int count = -1;  
  
    if (size != 0)  
        count = _vsnprintf_s(str, size, _TRUNCATE, format, ap);  
    if (count == -1)  
        count = _vscprintf(format, ap);  
  
    return count;  
}  
  
inline int c99_snprintf(char* str, size_t size, const char* format, ...)  
{  
    int count;  
    va_list ap;  
  
    va_start(ap, format);  
    count = c99_vsnprintf(str, size, format, ap);  
    va_end(ap);  
  
    return count;  
}  
#endif // _MSC_VER  




void *talker( void *d ) {
	int i = 0;
	char buf[220];

	char recv_buf[1024];
	int fd,hdr_len;

	fd = (int)d;

	printf("%d---\n",d);


    if (recv(fd,recv_buf,sizeof(recv_buf),0)< 0)   //接收错误
    {
        printf("%s\n",recv_buf);
		perror("read data from client");  
        exit(1);  
        return 0;
    }
	
/*
	while(1) {
		sprintf( buf, "HTTP/1.0 200 OK\r\n\r\nHello World!" );
		send( fd, buf, strlen( buf )+1,0 );
		Sleep(4);
	}
	*/

	//recv(fd,buf1,100,0);

	hdr_len = sprintf(buf, http_res_hdr_tmpl);
    send(fd, buf, hdr_len, 0);


	//sprintf( buf, "HTTP\/1.0 200 OK\\r\\n\\r\\nHello World!" );
	//send( fd, buf, strlen( buf )+1,0 );
	closesocket(d);   
	//free(buf);  
	//return NULL;  
}

int bindsockets() {
	int ss,s1,s2,n;
	fd_set sockfds;
	struct timeval tv;
	char buf1[256], buf2[256];
	struct sockaddr_in my_addr;

	struct sockaddr_in socks[50];

	pthread_t thread[50];
	int i=1;
	int resListen;
	

	snprintf( buf1, 14, "Hello world" );

	ss = socket( AF_INET, SOCK_STREAM, 0 );

	my_addr.sin_family = AF_INET; //将addr结构的属性定位为TCP/IP地址  
	my_addr.sin_port = htons( 9991 ); //将本地字节顺序转化为网络字节顺序。  
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY); //INADDR_ANY代表这个server上所有的地址  

	memset( &(my_addr.sin_zero), '\0', 8 ); // zero the rest

	if(setsockopt( ss, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(int) )==-1)
    {  
        printf("setsockopt failed %s\n", strerror(errno));  
        return EXIT_FAILURE;  
    }


	if (bind( ss, (struct sockaddr* )&my_addr, sizeof( struct sockaddr ) ) ==-1)
	{  
        printf("bind failed %s\n", strerror(errno));  
        return EXIT_FAILURE;  
    } 
	
	resListen =listen( ss, BACKLOG );
	
	if(resListen!=0)
	{  
		printf("failed to listen! \n");   
	}

	n = sizeof( struct sockaddr_in );
	i=0;
	while(1) {
		s1 = accept(ss,(struct sockaddr *)&socks[1], &n);
		pthread_create( &(thread[i++]), NULL, talker, (void *)s1 );
	}

	return(0);
}

//#ifdef SOCKTESTING
int main( int argc, char *argv[] ) {

#ifdef	WIN32
	WSADATA wsa_data;
	WSAStartup(0x0201, &wsa_data);
#endif

	bindsockets();

	return(0);
}
//#endif

/*

Server Software:        Mickel's
Server Hostname:        127.0.0.1
Server Port:            9991

Document Path:          /
Document Length:        12 bytes

Concurrency Level:      100
Time taken for tests:   2.036 seconds
Complete requests:      10000
Failed requests:        0
Total transferred:      1240000 bytes
HTML transferred:       120000 bytes
Requests per second:    4911.31 [#/sec] (mean)
Time per request:       20.361 [ms] (mean)
Time per request:       0.204 [ms] (mean, across all concurrent requests)
Transfer rate:          594.73 [Kbytes/sec] received

*/