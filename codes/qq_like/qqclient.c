/* 
 * qqclient.c 
 * 
 *  Created on: 2016年7月21日 
 *      Author: Administrator 
 */  
  
#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <errno.h>  
#include <unistd.h>  
#include <arpa/inet.h>  
#include <sys/types.h>          /* See NOTES */  
#include <sys/socket.h>  
#include <pthread.h>  
  
#define BUFSIZE 1024  
  
void *socket_read(void *arg)  
{  
    int st = *(int *)arg;  
    char buf[BUFSIZE];  
    while (1)  
    {  
        memset(buf, 0, sizeof(buf));  
        ssize_t rc = recv(st, buf, sizeof(buf), 0);  
        if (rc <= 0)  
        {  
            printf("recv failed, %s\n", strerror(errno));  
            break;  
        }  
        else  
        {  
            printf("recv '%s'\nrecv %u byte\n", buf, rc);  
        }  
    }  
    return NULL;  
}  
  
void *socket_write(void *arg)  
{  
    int st = *(int *)arg;  
    char buf[BUFSIZE];  
    while (1)  
    {  
        memset(buf, 0, sizeof(buf));  
        read(STDIN_FILENO, buf, sizeof(buf));  
        int ilen = strlen(buf);  
        if (buf[ilen - 1] == '\n')  
        {  
            buf[ilen - 1] = 0;  
        }  
        ssize_t rc = send(st, buf, sizeof(buf), 0);  
        printf("send '%s'\nsend %u byte\n", buf, rc);  
        if (rc <= 0)  
        {  
            printf("send failed, %s\n", strerror(errno));  
        }  
    }  
    return NULL;  
}  
  
int socket_connect(const char *hostname, int iport)  
{  
    int st = socket(AF_INET, SOCK_STREAM, 0);  
  
    struct sockaddr_in addr;  
    memset(&addr, 0, sizeof(addr));  
    addr.sin_family = AF_INET;  
    addr.sin_port = htons(iport);  
    addr.sin_addr.s_addr = inet_addr(hostname);  
  
    if (connect(st, (struct sockaddr *)&addr, sizeof(addr)) == -1)  
    {  
        printf("connect failed %s\n", strerror(errno));  
    }  
    else  
    {  
        printf("connect success\n");  
    }  
    return st;  
}  
  
  
int main(int arg, char *args[])  
{  
    if (arg < 3)  
    {  
        printf("usage:qqserver port\n");  
        return 0;  
    }  
  
    int iport = atoi(args[2]);  
    if (iport == 0)  
    {  
        printf("port %d is invalid\n", iport);  
        return 0;  
    }  
    printf("qqclient is begin\n");  
    int st = socket_connect(args[1], iport);  
    if (st == 0)  
        return 0;  
  
    pthread_t thr_read, thr_write;  
    pthread_create(&thr_read, NULL, socket_read, &st);// 启动读socket数据线程  
    pthread_create(&thr_write, NULL, socket_write, &st);// 启动写socket数据线程  
    pthread_join(thr_read, NULL);  
    //pthread_join(thr_write, NULL);// 如果等待thr_write退出，main函数可能被挂起  
    close(st);  
    printf("qqclient is end\n");  
    return 0;  
}  