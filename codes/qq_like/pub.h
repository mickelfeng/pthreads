/* 
 * pub.h 
 * 
 *  Created on: 2016��7��21�� 
 *      Author: Administrator 
 */  
  
#ifndef PUB_H_  
#define PUB_H_  
  
#include <stdio.h>  
#include <stdlib.h>  
  
  
  
void init_socket_client();  
void catch_Signal(int Sign);  
int signal1(int signo, void (*func)(int));  
void socket_accept(int st);  
int socket_create(int port);  
  
#endif /* PUB_H_ */  