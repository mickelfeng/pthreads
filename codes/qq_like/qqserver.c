/* 
 * qqserver.c 
 * 
 *  Created on: 2016年7月21日 
 *      Author: Administrator 
 */  
#include <sys/types.h>  
#include <sys/socket.h>    
#include "pub.h"  
  
int main(int arg, char *args[])  
{  
    if (arg < 2)  
    {  
        printf("usage:qqserver port\n");  
        return 0;  
    }  
  
    int iport = atoi(args[1]);  
    if (iport == 0)  
    {  
        printf("port %d is invalid\n", iport);  
        return 0;  
    }  
  
    printf("qqserver is begin\n");  
    //signal1(SIGINT, catch_Signal);// 捕捉SIGINT消息  
    init_socket_client();// 初始化init_socket_client[2]数组  
    int st = socket_create(iport);// 建立server端socket，在iport指定的断口号上listen  
    if (st == 0)  
        return 0;  
    socket_accept(st);  
    close(st);  
    printf("qqserver is end\n");  
    return 0;  
}