#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/shm.h>

#define PORT 22468
#define KEY 123
#define SIZE 1024

void handle_conn(void *client_sockfd);
int main()
{

    char buf[100];
    memset(buf,0,100);

    int server_sockfd,client_sockfd;
    socklen_t server_len,client_len;

    struct  sockaddr_in server_sockaddr,client_sockaddr;

    /*create a socket.type is AF_INET,sock_stream*/
    server_sockfd = socket(AF_INET,SOCK_STREAM,0);
    
    server_sockaddr.sin_family = AF_INET;
    server_sockaddr.sin_port = htons(PORT);
    server_sockaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    server_len = sizeof(server_sockaddr);
    
    int on;
    setsockopt(server_sockfd, SOL_SOCKET, SO_REUSEADDR,&on,sizeof(on));
    /*bind a socket or rename a sockt*/
    if(bind(server_sockfd, (struct sockaddr*)&server_sockaddr, server_len)==-1){
        printf("bind error");
        exit(1);
    }

    if(listen(server_sockfd, 5)==-1){
        printf("listen error");
        exit(1);
    }

    client_len = sizeof(client_sockaddr);

    pid_t ppid,pid;


	pthread_t thread_id;
    while(1) {


        if((client_sockfd = accept(server_sockfd, (struct sockaddr*)&client_sockaddr, &client_len)) == -1){
            printf("connect error");
            exit(1);
        } else {
            printf("create connection successfully\n");
			pthread_create(&thread_id, NULL, *handle_conn,(void *)client_sockfd);  
            
        }
	} 
    return 0;
}

/*
为每个客户端链接创建一个线程，就像golang 的go一样，开始我想实现 telnet 客户端输入换行符为一个协议包。结果没实现
*/

void handle_conn(void *client_sockfd){

	char recv_buf[1024];
	//memset(recv_buf,0,sizeof(recv_buf));  
	int n;

	//printf(client_sockfd);

	while ( 1 )  { 

		//int error = send(client_sockfd, "You have conected the server", strlen("You have conected the server"), 0);

		int pos=0;
		n=recv(client_sockfd,recv_buf,sizeof(recv_buf),0);
		/*
		while( n = recv(client_sockfd,recv_buf,sizeof(recv_buf),0) >0  )
		{
			if(strcmp(recv_buf,"\n")==0)  
				break; 
			pos++;
		}
		*/
		int error = send(client_sockfd, recv_buf, n, 0);

		puts(recv_buf);    

		if (n < 0) {
		   perror("Read error");   
		   //exit(1);  
		}  
		//printf("error   %d\n", error);

	}

}