
[测试Linux下socket每次发送数据和接受数据的最大值](https://blog.csdn.net/hellonigesb/article/details/51906420)

经试验发现，客户端一次发送数据大小有个上线，本机试验数据最大是384KB，超出最大值的时候，客户端会阻塞到发送语句哪里等待服务端接受数据。

服务多也是一样，有一个最大的一次性接受数据的上线，本机试验最大接受数据是64KB，但是这个服务端好像不确定具体接受多少

```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>

#define MAXBUF 1024 * 1024

void* recvsocket(void *arg)
{
	int st = *(int *)arg;
	char s[1024];

	while (1)
	{
		memset(s, 0, sizeof(s));
		int rc = recv(st, s, sizeof(s), 0);
		if (rc <= 0)
			break;
		printf("%s\n", s);
	}
	return NULL;
}

void* sendsocket(void *arg)
{
	int st = *(int *)arg;
	char s[1024];
	char *p = malloc(MAXBUF);//1M

	while (1)
	{
		memset(s, 0, sizeof(s));
		read(STDIN_FILENO, s, sizeof(s));
		int rc = send(st, p, MAXBUF, 0);
		printf("rc = %d\n", rc);
	}

	return NULL;
}

int main(int arg, char *args[])
{
	if (arg < 3)
	{
		return -1;
	}

	int port = atoi(args[2]);
	int st = socket(AF_INET, SOCK_STREAM, 0);//初始化socket

	struct sockaddr_in addr; // 定义一个IP地址的结构
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;// 设置结构地址类型为TCP/IP地址
	addr.sin_port = htons(port); // 制定一个端口号：8080，htons：将short类型从host字节类型转到net字节类型
	// 将字符类型的IP地址转化为int，赋给addr结构
	//addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_addr.s_addr = inet_addr(args[1]);

	//
	if (connect(st, (struct sockaddr *)&addr, sizeof(addr)) == -1)
	{
		printf("connect failed %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	pthread_t thrd1, thrd2;

	pthread_create(&thrd1, NULL, recvsocket, &st);
	pthread_create(&thrd2, NULL, sendsocket, &st);
	pthread_join(thrd1, NULL);
	//pthread_join(thrd2, NULL);// 加上这一句话会导致线程如法退出，主线程会一直等待子线程，子线程一直是退步出来的

	close(st);
	return EXIT_SUCCESS;
}

```

server

```
#define MAXBUF 1024 * 128

struct ps
{
	int st;
	pthread_t *thr;
};

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

int status = 0;

void* recvsocket(void *arg)
{
	struct ps *p = (struct ps *)arg;

	int st = p->st;
	char s[1024];
	char *pp = malloc(MAXBUF);
	while (1)
	{
		char buf[100];
		read(STDIN_FILENO, buf, sizeof(buf));
		memset(s, 0, sizeof(s));
		int rc = recv(st, pp, MAXBUF, 0);
		printf("rc = %d\n", rc);
		if (rc <= 0)//如果recv返回小于等于0，代表socket已经关闭或者出错了
			break;
		//printf("%s\n", s);
	}
	pthread_mutex_lock(&mutex);
	status = 0;
	pthread_mutex_unlock(&mutex);
	pthread_cancel(*(p->thr));
	return NULL;
}

void* sendsocket(void *arg)
{
	int st = *(int *)arg;
	char s[1024];

	while (1)
	{
		memset(s, 0, sizeof(s));
		read(STDIN_FILENO, s, sizeof(s));
		send(st, s, strlen(s), 0);
	}

	return NULL;
}

int main(int arg, char *args[])
{
	if (arg < 2)
	{
		return -1;
	}
	int port = atoi(args[1]);
	int st = socket(AF_INET, SOCK_STREAM, 0); //初始化socket

	int on = 1;
	if (setsockopt(st, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1)
	{
		printf("setsockopt failed %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	struct sockaddr_in addr; // 定义一个IP地址结构
	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET; // 将addr结构的属性定位为TCP/IP地址
	addr.sin_port = htons(port); // 将本地字节顺序转化为网络字节顺序
	addr.sin_addr.s_addr = htonl(INADDR_ANY); // INADDR_ANY代表这个server上所有的地址

	// 将ip与server程序绑定
	if (bind(st, (struct sockaddr *) &addr, sizeof(addr)) == -1)
	{
		printf("bind failed %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	// server端开始listen
	if (listen(st, 20) == -1)
	{
		printf("listen failed %s\n", strerror(errno));
		return EXIT_FAILURE;
	}
	//printf("listen success\n");
	int client_st = 0;
	struct sockaddr_in client_addr; // 表示client端的IP地址

	pthread_t thrd1, thrd2;
	while (1)
	{
		memset(&client_addr, 0, sizeof(client_addr));
		socklen_t len = sizeof(client_addr);
		client_st = accept(st, (struct sockaddr*) &client_addr, &len);

		pthread_mutex_lock(&mutex);
		status++;
		pthread_mutex_unlock(&mutex);
		if (status > 1)
		{
			close(client_st);
			continue;
		}
		if (client_st == -1)
		{
			printf("accept failed %s\n", strerror(errno));
			return EXIT_FAILURE;
		}
		printf("accept by %s\n", inet_ntoa(client_addr.sin_addr));
		struct ps ps1;
		ps1.st = client_st;
		ps1.thr = &thrd2;
		pthread_create(&thrd1, NULL, recvsocket, &ps1);
		pthread_detach(thrd1);//设置线程为可分离
		//pthread_create(&thrd2, NULL, sendsocket, &client_st);
		//pthread_detach(thrd2);//设置线程为可分离
	}
	close(st);
	return EXIT_SUCCESS;
}
```