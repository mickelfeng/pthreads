#include<stdio.h>
#include<stdlib.h>
#define BUFFSIZE 1024
#pragma comment(lib, "pthreadVC2.lib")
#include"thread_work.h"
#include"pub.h"

#ifndef WIN32

#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <signal.h>

#else

#include<ws2tcpip.h>

#endif

int main(int argc,char *argv[])
{
	int port = atoi(argv[1]);
	int st;

#ifdef	WIN32
	WSADATA wsa_data;
	WSAStartup(0x0201, &wsa_data);
#endif

	if(argc<2)
	{
		printf("usage:server port \n");
		return 0;
	}
	if(port <= 0)
	{
		printf("port must be positive integer: \n");
		return 0;
	}

	st =socket_create(port);
	if(st==0)
	{
		return 0;
	}

#ifndef WIN32

	setdaemon(); //设置为守护进程

#endif
	printf("my http server begin\n");
	socket_accept(st);
	close(st);
}
