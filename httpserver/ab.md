ab -c100 -n100000 http://localhost:9992/

```
Server Software:
Server Hostname:        127.0.0.1
Server Port:            9992

Document Path:          /
Document Length:        23 bytes

Concurrency Level:      100
Time taken for tests:   19.089 seconds
Complete requests:      100000
Failed requests:        0
Total transferred:      15200000 bytes
HTML transferred:       2300000 bytes
Requests per second:    5238.59 [#/sec] (mean)
Time per request:       19.089 [ms] (mean)
Time per request:       0.191 [ms] (mean, across all concurrent requests)
Transfer rate:          777.60 [Kbytes/sec] received
```