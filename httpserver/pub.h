
void save_log(char *buf);

#ifndef WIN32

void setdaemon(); //设置为守护进程

#endif


const char *get_filetype(const char *filename); //根据扩展名返回文件类型描述

int socket_create(int port);

int socket_accept(int st);


int  get_file_content(const char *file_name, char **content);// 得到文件内容
